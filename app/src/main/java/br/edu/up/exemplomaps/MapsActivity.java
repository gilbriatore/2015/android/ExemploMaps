package br.edu.up.exemplomaps;

import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.view.View;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class MapsActivity extends FragmentActivity
    implements OnMapReadyCallback,
    GoogleMap.OnMapClickListener, GoogleMap.OnMapLongClickListener{

  private GoogleMap mMap;
  private ArrayList<LatLng> locais;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    locais = new ArrayList<>();

    //Notifica o fragmento para carregar o mapa.
    SupportMapFragment smf = (SupportMapFragment)
        getSupportFragmentManager()
        .findFragmentById(R.id.map);
    smf.getMapAsync(this);
  }


  //https://maps.googleapis.com/maps/api/directions/json?origin=Ecoville&destination=Centro&key=AIzaSyCEqGQ4kQn-itA4Ze2KaDiWguJxdoHBFPQ";
  // latitude/longitude
  String origem = "-25.446844, -49.358866";
  String destino = "-25.4342824,-49.2883393";

  public void onClickTracarRota(View v){

    String origem = "-25.446844, -49.358866";
    String destino = "-25.4342824,-49.2883393";

    String url = "http://maps.google.com/maps?f=d&hl=en&saddr=%s&daddr=%s";
    Uri uri = Uri.parse(String.format(url, origem, destino));

    String view = android.content.Intent.ACTION_VIEW;
    Intent intent = new Intent(view, uri);
    startActivity(Intent.createChooser(intent, "Selecione"));
  }

  public void onClickBuscarProximo(View v){
    String query = "location=-25.445757,-49.350998&radius=5000&types=food&name=Amburguer";
    String url = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?%s&key=AIzaSyCEqGQ4kQn-itA4Ze2KaDiWguJxdoHBFPQ";
    url = String.format(url, query);

  }

  public void onClickBuscarEndereco(View v){

    String localOuCep = "Av. Luis Xavier, 125 - Curitiba, PR";
    Geocoder gc = new Geocoder(this);
    try {

      List<Address> enderecos = gc.getFromLocationName(localOuCep, 1);
      if (enderecos.size() > 0){
        Address endereco = enderecos.get(0);
        double latitude = endereco.getLatitude();
        double longitude = endereco.getLongitude();

        LatLng up = new LatLng(latitude, longitude);
        mMap.addMarker(new MarkerOptions().position(up).title("Sprada"));
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(up,16));

      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void onClickTracarRota2(View v){

    String origem = "-25.446844, -49.358866";
    String destino = "-25.4342824,-49.2883393";

    String url = "http://maps.google.com/maps?f=d&hl=en&saddr=%1&daddr=%2";
    Uri uri = Uri.parse(String.format(url, origem, destino));



    Intent intent = new Intent(android.content.Intent.ACTION_VIEW, uri);
    startActivity(Intent.createChooser(intent, "Selecione"));
  }




  /**
   * Método de retorno (callback) chamado, de forma
   * assíncrona, quando o mapa estiver pronto para uso.
   */
  @Override
  public void onMapReady(GoogleMap googleMap) {

//    mMap = googleMap;
//
//    //Adiciona o marcador em Sydney na Austrália;
//    LatLng sydney = new LatLng(-34, 151);
//    mMap.addMarker(new MarkerOptions().position(sydney).title("Sydney"));
//    mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));

    mMap = googleMap;
    mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
    mMap.getUiSettings().setZoomControlsEnabled(true);
    mMap.getUiSettings().setCompassEnabled(true);
    mMap.getUiSettings().setMapToolbarEnabled(true);

    //Coordenadas da Universidade Positivo;
    LatLng up = new LatLng(-25.446828, -49.358875);
    mMap.addMarker(new MarkerOptions().position(up).title("UP"));
    mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(up,16));
  }

  @Override
  public void onMapClick(LatLng latLng) {
    if (locais.size() >= 10){
      return;
    }
    if (locais.size() == 1){

    } else if(locais.size() == 2){

    } else {

    }
  }

  @Override
  public void onMapLongClick(LatLng latLng) {
    mMap.clear();
    locais.clear();
  }
}